import React from 'react';
// import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import { UnwrappedSearch } from '../containers/Search';

import { ShowCard } from '../containers/ShowCard';
import preload from '../data.json';
// talk about describe

test('Search renders correctly', () => {
  //   const component = renderer.create(<Search />);
  const component = shallow(
    <UnwrappedSearch shows={preload.shows} searchTerm="" />,
  );
  //   const tree = component.toJSON();
  //   expect(tree).toMatchSnapshot();
  expect(component).toMatchSnapshot();
});

xtest('Search should render correct amount of shows', () => {
  const component = shallow(<Search shows={preload.shows} />);
  expect(component.find(ShowCard).length).toEqual(preload.shows.length);
});

test('Search should render correct amount of shows based on search terms', () => {
  const searchWord = 'black';
  const component = shallow(
    <UnwrappedSearch shows={preload.shows} searchTerm={searchWord} />,
  );
  // component.find('input').simulate('change', { target: { value: searchWord } });
  const showCount = preload.shows.filter(({ title, description }) =>
    `${title} ${description}`.toUpperCase().includes(searchWord.toUpperCase()),
  ).length;

  expect(component.find(ShowCard).length).toEqual(showCount);
});
