import React from 'react';
import { hot } from 'react-hot-loader/root';
import { App } from './ClientApp';

const Root = () => <App />;

export default hot(Root);
